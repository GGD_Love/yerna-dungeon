local bootstrapper = require "game.bootstrapper"
local images = {}

local fileNames={
    background="assets/images/background.png",
    signModal="assets/images/wooden-sign768.png",
    
    barbarian="assets/images/barbarian.png",
    wall="assets/images/brick-wall.png",
    exit="assets/images/exit-door.png",
    floor="assets/images/floor.png",
    blueKey="assets/images/key_blue.png",
    redKey="assets/images/key_red.png",
    yellowKey="assets/images/key_yellow.png",
    blueLock="assets/images/padlock_blue.png",
    redLock="assets/images/padlock_red.png",
    yellowLock="assets/images/padlock_yellow.png",
    sign="assets/images/wooden-sign.png",
    cursor="assets/images/cursor.png",
    selected="assets/images/selected.png",
    
    houseButton="assets/images/editor/house.png",
    loadButton="assets/images/editor/load.png",
    playButton="assets/images/editor/play-button.png",
    saveButton="assets/images/editor/save.png",
    cancelButton="assets/images/editor/cancel.png",
    restartButton="assets/images/editor/clockwise-rotation.png",
    
    houseButtonHover="assets/images/editor/house_hover.png",
    loadButtonHover="assets/images/editor/load_hover.png",
    playButtonHover="assets/images/editor/play-button_hover.png",
    saveButtonHover="assets/images/editor/save_hover.png",
    cancelButtonHover="assets/images/editor/cancel_hover.png",
    restartButtonHover="assets/images/editor/clockwise-rotation_hover.png",
  }
local cache={}

function images.load()
  for k,v in pairs(fileNames) do
    cache[k]=love.graphics.newImage(v)
  end
  fileNames={}
end

function images.get(id)
  return cache[id]
end

bootstrapper.add(images.load)

return images