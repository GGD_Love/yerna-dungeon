local stage = require "game.stage"
local sceneControl = require "common.controls.scene"
local rectangleControl = require "common.controls.rectangle"
local animation = require "common.controls.animation"
local labelControl = require "common.controls.label"
local fonts = require "game.fonts"
local constants = require "game.constants"
local mapEditor = require "common.controls.mapeditor"
local level = require "game.level"
local buttonControl = require "common.controls.button"
local images = require "game.images"
local control = require "common.controls.control"
return {
  controls={},
  transitioning=false,
  nextState = "boot",
  paletteSelection="floor",
  start = function(self)
    if self.root == nil then
      self.root = sceneControl.new(stage,0,0,constants.screen().width(),constants.screen().height())
      self.controls.buttonPanel = sceneControl.new(stage,constants.screen().width(),704,256,64)
      self.controls.homeButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("houseButton"),hover=images.get("houseButtonHover")},0,0,64,64)
      self.controls.saveButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("saveButton"),hover=images.get("saveButtonHover")},64,0,64,64)
      self.controls.loadButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("loadButton"),hover=images.get("loadButtonHover")},128,0,64,64)
      self.controls.playButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("playButton"),hover=images.get("playButtonHover")},192,0,64,64)
      
      self.controls.palettePanel = sceneControl.new(stage,constants.mapView().width(),-704,256,704)
      self.controls.paletteFloor = buttonControl.new(self.controls.palettePanel,{normal=images.get("floor"),hover=images.get("floor")},8,8,48,48)
      self.controls.paletteWall = buttonControl.new(self.controls.palettePanel,{normal=images.get("wall"),hover=images.get("wall")},8+48,8,48,48)
      self.controls.paletteExit = buttonControl.new(self.controls.palettePanel,{normal=images.get("exit"),hover=images.get("exit")},8+96,8,48,48)
      self.controls.paletteBarbarian = buttonControl.new(self.controls.palettePanel,{normal=images.get("barbarian"),hover=images.get("barbarian")},8+144,8,48,48)
      self.controls.paletteRedKey = buttonControl.new(self.controls.palettePanel,{normal=images.get("redKey"),hover=images.get("redKey")},8+192,8,48,48)
      self.controls.paletteRedLock = buttonControl.new(self.controls.palettePanel,{normal=images.get("redLock"),hover=images.get("redLock")},8,8+48,48,48)
      self.controls.paletteYellowKey = buttonControl.new(self.controls.palettePanel,{normal=images.get("yellowKey"),hover=images.get("yellowKey")},8+48,8+48,48,48)
      self.controls.paletteYellowLock = buttonControl.new(self.controls.palettePanel,{normal=images.get("yellowLock"),hover=images.get("yellowLock")},8+96,8+48,48,48)
      self.controls.paletteBlueKey = buttonControl.new(self.controls.palettePanel,{normal=images.get("blueKey"),hover=images.get("blueKey")},8+144,8+48,48,48)
      self.controls.paletteBlueLock = buttonControl.new(self.controls.palettePanel,{normal=images.get("blueLock"),hover=images.get("blueLock")},8+192,8+48,48,48)
      self.controls.paletteSign = buttonControl.new(self.controls.palettePanel,{normal=images.get("sign"),hover=images.get("sign")},8,8+96,48,48)
      self.controls.paletteSelected = buttonControl.new(self.controls.palettePanel,{normal=images.get("selected"),hover=images.get("selected")},8,8,48,48)
    end
    if self.mapEditor == nil then
      self.mapEditor = mapEditor.new(self.root,constants.mapView().x(),constants.mapView().height(),constants.mapView().width(),constants.mapView().height(),constants.cell().width(),constants.cell().height())
    end
    if self.map == nil then
      self.map = level.new(constants.cell().columns(),constants.cell().rows(),true)
    end
    self.root:enable()
    self.mapEditor:addAnimation(animation.new("outBounce",1,{y={old=constants.mapView().height(),new=0}}))
    self.mapEditor:setMap(self.map)
    self.controls.buttonPanel:addAnimation(animation.new("outBounce",1,{x={old=constants.screen().width(),new=constants.mapView().height()}}))
    self.controls.palettePanel:addAnimation(animation.new("outBounce",1,{y={old=-704,new=0}}))
    self.transitioning=false
  end,
  draw = function(self)
    stage:draw()
  end,
  update = function(self, dt)
    stage:update(dt)
    if self.transitioning then
      if not stage:hasAnimation() then
        if self.mapEditor~=nil then
          self.mapEditor:setParent(nil)
          self.mapEditor=nil
        end
        self.stateMachine:setCurrent(self.nextState)
        self.root:disable()
      end
    end
  end,
  mousepressed = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if button==1 and control.getHover()~=nil and control.getHover().getMap~=nil then
        if self.paletteSelection=="barbarian" then
          local bx, by = control.getHover():getMap():findPlayer()
          if bx~=nil then
            control.getHover():getMap():setCell(bx,by,"floor")
          end
        end
        control.getHover():getMap():setCell(control.getHover():getMapColumn(),control.getHover():getMapRow(),self.paletteSelection)
      end    
    end
  end,
  mousereleased = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if self.controls.homeButton:isHover() then
        self.nextState="title"
        self:startTransition()
      elseif self.controls.saveButton:isHover() then
        self.map:save("level.txt")
      elseif self.controls.loadButton:isHover() then
        local map = level.load("level.txt")
        if map~=nil then
          self.map=map
          self.mapEditor:setMap(self.map)
        end
      elseif self.controls.playButton:isHover() then
        self.stateMachine:getState("testlevel"):setMap(level.copy(self.map))
        self.nextState="testlevel"
        self:startTransition()
      elseif self.controls.paletteFloor:isHover() then
        self.paletteSelection="floor"
        self.controls.paletteSelected:setLocalX(self.controls.paletteFloor:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteFloor:getLocalY())
      elseif self.controls.paletteExit:isHover() then
        self.paletteSelection="exit"
        self.controls.paletteSelected:setLocalX(self.controls.paletteExit:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteExit:getLocalY())
      elseif self.controls.paletteWall:isHover() then
        self.paletteSelection="wall"
        self.controls.paletteSelected:setLocalX(self.controls.paletteWall:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteWall:getLocalY())
      elseif self.controls.paletteBarbarian:isHover() then
        self.paletteSelection="barbarian"
        self.controls.paletteSelected:setLocalX(self.controls.paletteBarbarian:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteBarbarian:getLocalY())
      elseif self.controls.paletteRedKey:isHover() then
        self.paletteSelection="redKey"
        self.controls.paletteSelected:setLocalX(self.controls.paletteRedKey:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteRedKey:getLocalY())
      elseif self.controls.paletteRedLock:isHover() then
        self.paletteSelection="redLock"
        self.controls.paletteSelected:setLocalX(self.controls.paletteRedLock:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteRedLock:getLocalY())
      elseif self.controls.paletteYellowKey:isHover() then
        self.paletteSelection="yellowKey"
        self.controls.paletteSelected:setLocalX(self.controls.paletteYellowKey:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteYellowKey:getLocalY())
      elseif self.controls.paletteYellowLock:isHover() then
        self.paletteSelection="yellowLock"
        self.controls.paletteSelected:setLocalX(self.controls.paletteYellowLock:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteYellowLock:getLocalY())
      elseif self.controls.paletteBlueKey:isHover() then
        self.paletteSelection="blueKey"
        self.controls.paletteSelected:setLocalX(self.controls.paletteBlueKey:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteBlueKey:getLocalY())
      elseif self.controls.paletteBlueLock:isHover() then
        self.paletteSelection="blueLock"
        self.controls.paletteSelected:setLocalX(self.controls.paletteBlueLock:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteBlueLock:getLocalY())
      elseif self.controls.paletteSign:isHover() then
        self.paletteSelection="sign"
        self.controls.paletteSelected:setLocalX(self.controls.paletteSign:getLocalX())
        self.controls.paletteSelected:setLocalY(self.controls.paletteSign:getLocalY())
      elseif control.getHover()~=nil and control.getHover().getMap~=nil then
        if self.paletteSelection=="barbarian" then
          local bx, by = control.getHover():getMap():findPlayer()
          if bx~=nil then
            control.getHover():getMap():setCell(bx,by,"floor")
          end
        end
        control.getHover():getMap():setCell(control.getHover():getMapColumn(),control.getHover():getMapRow(),self.paletteSelection)
      end
    end
  end,
  mousemoved = function(self, x, y, dx, dy, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if love.mouse.isDown(1) and control.getHover()~=nil and control.getHover().getMap~=nil then
        if self.paletteSelection=="barbarian" then
          local bx, by = control.getHover():getMap():findPlayer()
          if bx~=nil then
            control.getHover():getMap():setCell(bx,by,"floor")
          end
        end
        control.getHover():getMap():setCell(control.getHover():getMapColumn(),control.getHover():getMapRow(),self.paletteSelection)
      end    
    end
  end,
  startTransition = function(self)
    self.transitioning=true
    for k,v in pairs(self.controls) do
      v:clearAnimations()
    end
    self.mapEditor:addAnimation(animation.new("outBounce",1,{y={new=constants.mapView().height(),old=0}}))
    self.controls.buttonPanel:addAnimation(animation.new("outBounce",1,{x={new=constants.screen().width(),old=constants.mapView().height()}}))
    self.controls.palettePanel:addAnimation(animation.new("outBounce",1,{y={old=0,new=-704}}))
  end
}
