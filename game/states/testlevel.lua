local stage = require "game.stage"
local sceneControl = require "common.controls.scene"
local rectangleControl = require "common.controls.rectangle"
local animation = require "common.controls.animation"
local labelControl = require "common.controls.label"
local fonts = require "game.fonts"
local constants = require "game.constants"
local buttonControl = require "common.controls.button"
local images = require "game.images"
local level = require "game.level"
local mapPlayer = require "common.controls.mapplayer"
local imageControl = require "common.controls.image"
local inventoryControl = require "common.controls.inventory"
return {
  controls={
  },
  transitioning=false,
  nextState = "boot",
  setMap = function(self,map)
    self.restartMap = level.copy(map)
    self.map=map
    self.inventory={}
    if self.controls.player~=nil then
      self.controls.player:setMap(self.map)
    end
    self:refreshInventory()
  end,
  start = function(self)
    if self.root == nil then
      self.root = sceneControl.new(stage,0,0,constants.screen().width(),constants.screen().height())
      
      self.controls.player = mapPlayer.new(self.root,-constants.mapView().width(),0,constants.mapView().width(),constants.mapView().height())
      
      --self.controls.inventory = inventoryControl.new(self.root,constants.screen().width(),0,256,256,5,5)
      self.controls.inventory = inventoryControl.new(self.root,768,0,256,256,5,5)
      imageControl.new(self.controls.inventory,images.get("barbarian"),0,0,48,48)
      
      self.controls.buttonPanel = sceneControl.new(stage,constants.screen().width(),704,256,64)
      self.controls.restartButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("restartButton"),hover=images.get("restartButtonHover")},0,0,64,64)
      self.controls.cancelButton = buttonControl.new(self.controls.buttonPanel,{normal=images.get("cancelButton"),hover=images.get("cancelButtonHover")},192,0,64,64)
      
      self.controls.signPanel = imageControl.new(self.root,images.get("signModal"),0,0,768,768)
      self.controls.signText = labelControl.new(self.controls.signPanel,fonts.get("default"),"test","center",100,200,568,300,{255,255,255,255},{255,255,255,255})
    end
    self.controls.player:setMap(self.map)
    self.root:enable()
    self.transitioning=false
    self.controls.player:addAnimation(animation.new("outBounce",1,{x={old=-constants.mapView().width(),new=0}}))
    self.controls.buttonPanel:addAnimation(animation.new("outBounce",1,{x={old=constants.screen().width(),new=constants.mapView().height()}}))
    --self.controls.inventory:addAnimation(animation.new("outBounce",1,{x={old=constants.screen().width(),new=768}}))
    self.controls.inventory:removeChildren()
    self.controls.signPanel:disable()
  end,
  draw = function(self)
    stage:draw()
  end,
  update = function(self, dt)
    stage:update(dt)
    if self.transitioning then
      if not stage:hasAnimation() then
        self.root:disable()
        self.stateMachine:setCurrent(self.nextState)
      end
    end
  end,
  mousereleased = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if self.controls.cancelButton:isHover() then
        self.nextState="editor"
        self:startTransition()
      elseif self.controls.restartButton:isHover() then
        self:setMap(self.restartMap)
      end
    end
  end,
  mousemoved = function(self, x, y, dx, dy, istouch)
    stage:checkHover(x,y)
  end,
  refreshInventory = function (self)
    if self.controls.inventory~=nil then
      self.controls.inventory:updateInventory(self.inventory)
      --self.controls.inventory:removeChildren()
      --local column=1
      --local row=1
      --for k,v in pairs(self.inventory) do
        --local counter = v
        --while counter > 0 do
          --counter = counter - 1
          --local x = (column-1) * constants.cell().width()+8
          --local y = (row-1) * constants.cell().height()+8
          --imageControl.new(self.controls.inventory,images.get(k),x,y,constants.cell().width(),constants.cell().height())
          --column = column + 1
          --if column > 5 then
            --column = column - 5
            --row = row + 1
          --end
        --end
      --end
    end
  end,
  movePlayer = function (self, dx, dy)
    local signText = self.map:getSignText(dx,dy)
    local nextLevel = self.map:getNextLevel(dx,dy)
    if signText~=nil then
      self.controls.signPanel:enable()
      self.controls.signText:setText(signText)
    end
    local newInventory, newItem = self.map:makeMove(dx,dy,self.inventory)
    self.inventory=newInventory
    self.controls.player:updatePlayer()
    self:refreshInventory()
  end,
  keypressed = function(self, key, scancode, isrepeat) 
    if not stage:hasAnimation() then
      if not self.controls.signPanel:isEnabled() then
        if key=="left" then
          self:movePlayer(-1,0)
        elseif key=="right" then
          self:movePlayer(1,0)
        elseif key=="up" then
          self:movePlayer(0,-1)
        elseif key=="down" then
          self:movePlayer(0,1)
        end
      else
        if key=="escape" then
          self.controls.signPanel:disable()
        end
      end
    end
  end,
  startTransition = function(self)
    self.transitioning=true
    for k,v in pairs(self.controls) do
      v:clearAnimations()
    end
    self.controls.buttonPanel:addAnimation(animation.new("outBounce",1,{x={new=constants.screen().width(),old=constants.mapView().height()}}))
    self.controls.player:addAnimation(animation.new("outBounce",1,{x={new=-constants.mapView().width(),old=0}}))
    --self.controls.inventory:addAnimation(animation.new("outBounce",1,{x={new=constants.screen().width(),old=768}}))
  end
}
