local states = {
  boot="game.states.boot",
  title="game.states.title",
  editor="game.states.editor",
  testlevel="game.states.testlevel",
  options="game.states.options",
  about="game.states.about",
}
local stateMachine={
  current="boot",
  states={},
  addState=function(self,stateName,state)
    self.states[stateName]=state
    state.stateMachine=self
  end,
  getState=function(self,stateName)
    return self.states[stateName]
  end,
  setCurrent=function(self,stateName)
    if stateName~=self.current then
      if self.states[self.current].finish~=nil then
        self.states[self.current]:finish()
      end
      self.current = stateName
      if self.states[self.current].start~=nil then
        self.states[self.current]:start()
      end
    end
  end,
  setImages = function(self,images)
    for _,v in pairs(self.states) do
      v.images = images
    end
  end,
  draw = function (self) 
    if self.states[self.current].draw~=nil then
      self.states[self.current]:draw()
    end
  end,
  update = function(self,dt) 
    if self.states[self.current].update~=nil then
      self.states[self.current]:update(dt)
    end
  end,
  mousepressed = function(self,x,y,button,istouch) 
    if self.states[self.current].mousepressed~=nil then
      self.states[self.current]:mousepressed(x,y,button,istouch)
    end
  end,
  mousereleased = function(self,x,y,button,istouch) 
    if self.states[self.current].mousereleased~=nil then
      self.states[self.current]:mousereleased(x,y,button,istouch)
    end
  end,
  mousemoved = function(self,x,y,dx,dy,istouch) 
    if self.states[self.current].mousemoved~=nil then
      self.states[self.current]:mousemoved(x,y,button,istouch)
    end
  end,
  wheelmoved = function(self,x,y) 
    if self.states[self.current].wheelmoved~=nil then
      self.states[self.current]:wheelmoved(x,y)
    end
  end,
  filedropped = function(self,file) 
    if self.states[self.current].filedropped~=nil then
      self.states[self.current]:filedropped(file)
    end
  end,
  focus = function(self,focus) 
    if self.states[self.current].focus~=nil then
      self.states[self.current]:focus(focus)
    end
  end,
  visible = function(self,visible) 
    if self.states[self.current].visible~=nil then
      self.states[self.current]:visible(visible)
    end
  end,
  mousefocus = function(self,focus) 
    if self.states[self.current].mousefocus~=nil then
      self.states[self.current]:mousefocus(focus)
    end
  end,
  keypressed = function(self,key, scancode, isrepeat) 
    if self.states[self.current].keypressed~=nil then
      self.states[self.current]:keypressed(key, scancode, isrepeat)
    end
  end,
  keyreleased = function(self,key, scancode) 
    if self.states[self.current].keyreleased~=nil then
      self.states[self.current]:keyreleased(key, scancode)
    end
  end,
  resize = function(self,w, h) 
    if self.states[self.current].resize~=nil then
      self.states[self.current]:resize(w, h)
    end
  end,
  textedited = function(self,text, start, length) 
    if self.states[self.current].textedited~=nil then
      self.states[self.current]:textedited(text, start, length)
    end
  end,
  textinput = function(self,text) 
    if self.states[self.current].textinput~=nil then
      self.states[self.current]:textinput(text)
    end
  end,
}
for k,v in pairs(states) do
  stateMachine:addState(k,require(v))
end
return stateMachine