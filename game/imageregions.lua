local bootstrapper = require "game.bootstrapper"
local images = require "game.images"
local rectangle = require "common.rectangle"
local imageregions = {}
local imageregion = require "common.imageregion"

local regions={
    --{image="barbarian",bounds=rectangle.new(0,0,128,128)},
  }
local cache={}

function imageregions.load()
  for k,v in ipairs(regions) do
    cache[k]=imageregion.new(images.get(v.image),v.bounds)
  end
  regions={}
end

function imageregions.get(id)
  return cache[id]
end

bootstrapper.add(imageregions.load)

return imageregions