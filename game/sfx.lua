local options = require "game.options"
local bootstrapper = require "game.bootstrapper"
local sfx={}

local fileNames={
  --button="assets/audio/button.wav",
}
local cache={}

function sfx.load()
  for k,v in pairs(fileNames) do
    cache[k]=love.audio.newSource(v,"static")
  end
  fileNames={}
end

function sfx.play(id)
  if options.get("audio") then
    cache[id]:play()
  end
end

bootstrapper.add(sfx.load)

return sfx