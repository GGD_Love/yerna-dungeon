local unitTest = require "utilities.unittest"

--bootstrapper library object
--initialize to empty object, as is my habit
local bootstrapper = {}

--static data for this library is hidden from the outside
--starts as empty table
--gets changed to nil after a call to bootstrapper.load
local loaders={}

--bootstrapper.add
--adds a function to the bootstrapper
--requires that the loader parameter is a function
--will throw an exception when called after bootstrapper.load
function bootstrapper.add(loader)
  assert(type(loader)=="function","bootstrapper.add - loader must be a function")
  assert(type(loaders)=="table","bootstrapper.add - cannot call after bootstrapper.load hase been called")
  
  --idempotency...
  --if the loader already exists in loaders, don't re-add it and just return
  for _,v in ipairs(loaders) do
    if v == loader then
      return
    end
  end
  
  table.insert(loaders,loader)
end

function bootstrapper.load()
  assert(type(loaders)=="table","bootstrapper.load - cannot call multiple times")
  
  --perform the load actions
  for _,v in ipairs(loaders) do
    v()
  end
  
  --eliminate loaders to keep from being re-called
  loaders=nil
end

unitTest.run("bootstrapper",function() 
		--save off old static state
		local oldLoaders = loaders
		loaders = {}
		
		--bootstrapper.add with non-function
		local status, err = pcall(function() bootstrapper.add(nil) end)
		assert(not status)
		
		local counter = 0
		local called1 = false
		local called2 = false
		function f1() 
			called1 = true
			counter = counter + 1 
		end
		function f2() 
			called2 = true
			counter = counter + 1 
		end
		
		--bootstrapper.add with function
		bootstrapper.add(f1)
		assert(#loaders==1)
		--bootstrapper.add with same function
		bootstrapper.add(f1)
		assert(#loaders==1)
		--bootstrapper.add with second function
		bootstrapper.add(f2)
		assert(#loaders==2)
		--bootstrapper.load
		bootstrapper.load()
		assert(loaders==nil)
		assert(called1)
		assert(called2)
		assert(counter==2)
		
		--bootstrapper.add after load
		status, err = pcall(function() bootstrapper.add(f1) end)
		assert(not status)
		--bootstrapper.load after load
		status, err = pcall(function() bootstrapper.load() end)
		assert(not status)
		
		--restore original static state
		loaders = oldLoaders
	end)
	
local mt = {
	__index=function (t,k) 
		return bootstrapper[k]
	end,
	__newindex=function(t,k,v) end,
  __metatable="bootstrapper"
}

local proxy = {}
setmetatable(proxy,mt)

return proxy