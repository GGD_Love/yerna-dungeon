local json = require "lunajson"
local bootstrapper = require "game.bootstrapper"
local proxy = require "utilities.proxy"
local platform = require "core.platform"

local options = {}--library

local fileName="options.txt"
local descriptors = {
    audio={default=function() return true end, validation=function(v) return type(v)=="boolean" end}
}

local values = {}

function options.load()
  local data = platform.readFile(fileName)
  if data==nil then
    return
  end
  local newValues = json.decode(data)--TODO: what happens when parsing fails?
  for k,v in pairs(newValues) do
    --option files exist in a place where they can be manipulated manually.... 
    --loading should be fault tolerant and ignore bad keys and values
    if descriptors[k]~=nil and descriptors[k].validation(v) then
      options.set(k,v)
    end
  end
end

function options.save()
  platform.writeFile(fileName,json.encode(values))
end

function options.get(id)
  assert(type(id)=="string","options.get - id must be a string")
  assert(descriptors[id]~=nil,"options.get - option not recognized - '"..id.."'")
  if values[id]==nil then
    return descriptors[id].default()
  else
    return values[id]
  end
end

function options.set(id,value)
  assert(type(id)=="string","options.set - id must be a string")
  assert(descriptors[id]~=nil,"options.set - option not recognized - '"..id.."'")
  assert(descriptors[id].validate(value),"options.set - invalid value for option '"..id.."' of '"..tostring(value).."'")
  --TODO: make a copy if it is a table?
  values[id]=value
  if descriptors[id].listeners~=nil then
    for _,v in ipairs(descriptors[id].listeners) do
      v()
    end
  end
end

function options.addListener(id, listener)
  assert(type(id)=="string","options.addListener - id must be a string")
  assert(descriptors[id]~=nil,"options.addListener - option not recognized - '"..id.."'")
  assert(type(listener)=="function","options.addListener - listener must be a function")
  descriptors[id].listeners = descriptors[id].listeners or {}
  --TODO: these next lines of code are "add item to table if the item isn't already there" and should be a utility function
  for _,v in ipairs(descriptors[id].listeners) do
    if v == listener then
      return
    end
  end
  table.insert(descriptors[id].listeners,listener)
end

function options.removeListener(id, listener)
  assert(type(id)=="string","options.removeListener - id must be a string")
  assert(descriptors[id]~=nil,"options.removeListener - option not recognized - '"..id.."'")
  assert(type(listener)=="function","options.removeListener - listener must be a function")
  if descriptors[id].listeners~=nil then
    --TODO: these lines of code are "remove items from table that are equal to a value" and should be a utility function
    local removeMe = {}
    for i,v in ipairs(descriptors[id].listeners) do
      if v == listener then
        table.insert(removeMe,1,i)
      end
    end
    for _,v in ipairs(removeMe) do
      table.remove(descriptors[id].listeners,v)
    end
  end
end

--TODO: unit tests

bootstrapper.add(options.load)--add to the bootstrapper

return proxy.newProxy(options,"options")--proxify