local options = require "game.options"
local bootstrapper = require "game.bootstrapper"
local music={}

local fileNames={
  default = "assets/audio/Komiku_-_14_-_The_Ultimate_Threat.mp3"
}
local cache={}

function music.load()
  for k,v in pairs(fileNames) do
    cache[k]=love.audio.newSource(v,"stream")
    cache[k]:setLooping(true)
    cache[k]:setVolume(0.5)
  end
  fileNames={}
end

function music.play(id)
  if options.get("audio") then
    cache[id]:play()
  end
end

function music.applyOptions()
  if options.get("audio") then
    cache["default"]:play()
  else
    for k,v in pairs(cache) do
      v:stop()
    end
  end
end

bootstrapper.add(music.load)
bootstrapper.add(music.applyOptions)

return music