local bootstrapper = require "game.bootstrapper"
local fonts = {}

local fileNames={
    default={fileName="assets/fonts/Ancient Runes_1_1.ttf", size=40}
  }
local cache={}

function fonts.load()
  for k,v in pairs(fileNames) do
    cache[k]=love.graphics.newFont(v.fileName,v.size)
  end
  fileNames={}
end

function fonts.get(id)
  return cache[id]
end

bootstrapper.add(fonts.load)

return fonts