local unittest = {}

local configuration = {
  enabled=true
}

function unittest.run(name,test)
  assert(type(name)=="string","unittest.run - name must be a string")
  assert(type(test)=="function","unittest.run - test must be a function")
  if configuration.enabled then
    local start = os.clock()
    local status, err = pcall(test)
    local elapsed = os.clock()-start
    if status then
      print(string.format("unittest.run\n\tname:%s\n\telapsed:%f\n\tPASSED\n\n",name,elapsed))
    else
      print(string.format("unittest.run\n\tname:%s\n\telapsed:%f\n\tFAILED - %s\n\n",name,elapsed,err))
    end
  end
end

return require("utilities.proxy").newProxy(unittest)