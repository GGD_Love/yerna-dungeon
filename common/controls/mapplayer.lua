local control = require "common.controls.control"
local constants = require "game.constants"
local images = require "game.images"
local imageControl = require "common.controls.image"
local animation = require "common.controls.animation"
local mapPlayer = {}

function mapPlayer.new(parent,x,y,width,height)
  assert(type(x)=="number","mapPlayer.new - x must be a number")
  assert(type(y)=="number","mapPlayer.new - y must be a number")
  assert(type(width)=="number","mapPlayer.new - width must be a number")
  assert(type(height)=="number","mapPlayer.new - height must be a number")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  function instance:preDraw()
    love.graphics.setScissor(self:getX(),self:getY(),self:getWidth(),self:getHeight())
  end
  
  function instance:postDraw()
    love.graphics.setScissor()
  end
  
  function instance:setMap(map)
    self.map = map
    if self.map~=nil then
      self:removeChildren()
      if self.playerImage~=nil then
        self.playerImage:setParent(nil)
        self.playerImage = nil
      end
      self.imageGrid={}
      local playerColumn,playerRow = self.map:findPlayer()
      for column=1,self.map:getColumns() do
        self.imageGrid[column]={}
        for row=1,self.map:getRows() do
          local x = constants.cell().width() * (column-1.5)
          local y = constants.cell().height() * (row-1.5)
          if column==playerColumn and row==playerRow then
            self.imageGrid[column][row]=imageControl.new(self,images.get("floor"),x,y,constants.cell().width(),constants.cell().height())
          else
            self.imageGrid[column][row]=imageControl.new(self,images.get(self.map:getCell(column,row)),x,y,constants.cell().width(),constants.cell().height())
          end
        end
      end
      local x = constants.cell().width() * (playerColumn-1.5)
      local y = constants.cell().height() * (playerRow-1.5)
      self.playerImage = imageControl.new(self,images.get("barbarian"),x,y,constants.cell().width(),constants.cell().height())
    end
  end
  
  function instance:updatePlayer()
    local playerColumn,playerRow = self.map:findPlayer()
    self.imageGrid[playerColumn][playerRow]:setImage(images.get("floor"))
    local x = constants.cell().width() * (playerColumn-1.5)
    local y = constants.cell().height() * (playerRow-1.5)
    self.playerImage:addAnimation(animation.new("linear",0.25,{x={new=x,old=self.playerImage:getLocalX()},y={new=y,old=self.playerImage:getLocalY()}}))
  end
  
  return instance
end

return mapPlayer