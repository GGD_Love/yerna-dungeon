local control={}

local mt = {
  __metatable="control"
}

local statics = {
}

function control.getHover()
  return statics.hover
end


function control.new(parent)
  assert(type(parent)=="nil" or (type(parent)=="table" and getmetatable(parent)==mt.__metatable),"control.new - parent must be nil or a control")
  local instance = {}
  setmetatable(instance,mt)
  
  local values = {
    enabled=true,
    children = {},
    animations={},
    deleteMe = false,
    locals = {
      x=0,
      y=0,
      w=0,
      h=0,
      r=255,
      g=255,
      b=255,
      a=255
    }
  }
  
  function instance:enable()
    values.enabled=true
  end
  
  function instance:disable()
    values.enabled=false
  end
  
  function instance:isEnabled()
    return values.enabled
  end
  
  function instance:addAnimation(animation)
    assert(type(animation)=="table" and getmetatable(animation)=="animation","control:addAnimation - animation must be an animation")
    table.insert(values.animations,animation)
  end
  
  function instance:hasAnimation()
    if #values.animations>0 then
      return true
    else
      for _,v in ipairs(values.children) do
        if v:hasAnimation() then
          return true
        end
      end
    end
    return false
  end
  
  function instance:clearAnimations()
    values.animations={}
  end
  
  
  function instance:setLocal(key,value)
    assert(type(key)=="string","control:setLocal - key must be a string")
    assert(type(value)=="number","control:setLocal - value must be a number")
    values.locals[key]=value
  end
  
  function instance:getLocal(key)
    assert(type(key)=="string","control:getLocal - key must be a string")
    return values.locals[key]
  end
  
  function instance:setLocalX(x)
    assert(type(x)=="number","control:setLocalX - x must be a number")
    values.locals.x=x
  end
  
  function instance:setLocalY(y)
    assert(type(y)=="number","control:setLocalY - y must be a number")
    values.locals.y=y
  end
  
  function instance:setLocalWidth(w)
    assert(type(w)=="number","control:setLocalWidth - w must be a number")
    values.locals.w=w
  end
  
  function instance:setLocalHeight(h)
    assert(type(h)=="number","control:setLocalHeight - h must be a number")
    values.locals.h=h
  end
  
  function instance:setLocalRed(r)
    assert(type(r)=="number","control:setLocalRed - r must be a number")
    values.locals.r=r
  end
  
  function instance:setLocalGreen(g)
    assert(type(g)=="number","control:setLocalGreen - g must be a number")
    values.locals.g=g
  end
  
  function instance:setLocalBlue(b)
    assert(type(b)=="number","control:setLocalBlue - b must be a number")
    values.locals.b=b
  end
  
  function instance:setLocalAlpha(a)
    assert(type(a)=="number","control:setLocalAlpha - a must be a number")
    values.locals.a=a
  end
  
  function instance:getLocalX()
    return values.locals.x
  end
  
  function instance:getLocalY()
    return values.locals.y
  end
  
  function instance:getLocalWidth()
    return values.locals.w
  end
  
  function instance:getLocalHeight()
    return values.locals.h
  end
  
  function instance:getLocalRed()
    return values.locals.r
  end
  
  function instance:getLocalGreen()
    return values.locals.g
  end
  
  function instance:getLocalBlue()
    return values.locals.b
  end
  
  function instance:getLocalAlpha()
    return values.locals.a
  end
  
  function instance:getX()
    if self:hasParent() then
      return self:getParent():getX() + self:getLocalX()
    else
      return self:getLocalX()
    end
  end
  
  function instance:getY()
    if self:hasParent() then
      return self:getParent():getY() + self:getLocalY()
    else
      return self:getLocalY()
    end
  end
  
  function instance:getWidth()
    return self:getLocalWidth()
  end
  
  function instance:getHeight()
    return self:getLocalHeight()
  end
  
  function instance:getRed()
    return self:getLocalRed()
  end
  
  function instance:getGreen()
    return self:getLocalGreen()
  end
  
  function instance:getBlue()
    return self:getLocalBlue()
  end
  
  function instance:getAlpha()
    return self:getLocalAlpha()
  end
  
  function instance:getParent()
    return values.parent
  end
  
  function instance:hasParent()
    return self:getParent()~=nil
  end
  
  function instance:setParent(parent)
    assert(type(parent)=="nil" or (type(parent)=="table" and getmetatable(parent)==mt.__metatable),"control:setParent - parent must be nil or a control")
    if self:hasParent() then
      self:getParent():removeChild(self)
    end
    values.parent = parent
    if self:hasParent() then
      self:getParent():addChild(self)
    end
  end
  
  function instance:addChild(child)
    assert(type(child)=="table" and getmetatable(child)==mt.__metatable,"control:addChild - child must be a control")
    assert(child:getParent()==self,"control:addChild - child cannot be added to a non-parent")
    self:removeChild(child)
    table.insert(values.children,child)
  end
  
  function instance:removeChild(child)
    assert(type(child)=="table" and getmetatable(child)==mt.__metatable,"control:removeChild - child must be a control")
    assert(child:getParent()==self,"control:removeChild - child cannot be removed from a non-parent")
    local removeMe = {}
    for i,v in ipairs(values.children) do
      if v==child then
        table.insert(removeMe,1,i)
      end
    end
    for _,v in ipairs(removeMe) do
      table.remove(values.children,v)
    end
  end
  
  function instance:removeChildren()
    while #values.children>0 do
      self:removeChild(values.children[1])
    end
  end
  
  function instance:markDeleted()
    values.deleteMe = true
  end
  
  function instance:isMarkedDeleted()
    return values.deleteMe
  end
  
  function instance:preDraw()
  end
  
  function instance:onDraw()
  end
  
  function instance:postDraw()
  end
  
  function instance:draw()
    if self:isEnabled() then
      self:preDraw()
      self:onDraw()
      for _,v in ipairs(values.children) do
        v:draw()
      end
      self:postDraw()
    end
  end
  
  function instance:onUpdate(dt)
  end
  
  function instance:update(dt)
    if self:isEnabled() then
      local adt = dt
      while #values.animations > 0 and adt > 0 do
        adt = values.animations[1]:update(adt)
        local result = values.animations[1]:getValues()
        for k,v in pairs(result) do
          values.locals[k]=v
        end
        if adt>0 then
          local animation = values.animations[1]
          table.remove(values.animations,1)
          if animation:autoRepeats() then
            animation:reset()
            self:addAnimation(animation)
          elseif animation:deletesAfter() then
            self:markDeleted()
          end
        end
      end
      self:onUpdate(dt)
      local deleteQueue={}
      for _,v in ipairs(values.children) do
        if v:isMarkedDeleted() then
          table.insert(deleteQueue,1,v)
        else
          v:update(dt)
        end
      end
      for i,v in ipairs(deleteQueue) do
        v:setParent(nil)
      end
    end
  end
  
  function instance:checkHover(x,y)
    if self:getParent()==nil then
      statics.hover = nil
    else
      if statics.hover~=nil or not self:isEnabled() then
        return
      end
    end
    if #values.children>0 then
      for index=#values.children,1,-1 do
        values.children[index]:checkHover(x,y)
        if statics.hover~=nil then
          return
        end
      end
    end
    if x>=self:getX() and y>=self:getY() and x<self:getX()+self:getWidth() and y<self:getY()+self:getHeight() then
      statics.hover = self
    end
  end
  
  function instance:isHover()
    return self==statics.hover
  end
  
  instance:setParent(parent)
  
  return instance
end

return control