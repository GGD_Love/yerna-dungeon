local control = require "common.controls.control"
local scene = {}

function scene.new(parent,x,y,width,height)
  assert(type(x)=="number","scene.new - x must be a number")
  assert(type(y)=="number","scene.new - y must be a number")
  assert(type(width)=="number","scene.new - width must be a number")
  assert(type(height)=="number","scene.new - height must be a number")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  function instance:preDraw()
    love.graphics.setScissor(self:getX(),self:getY(),self:getWidth(),self:getHeight())
  end
  
  function instance:postDraw()
    love.graphics.setScissor()
  end
  
  return instance
end

return scene