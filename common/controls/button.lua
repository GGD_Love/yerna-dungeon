local control = require "common.controls.control"
local button = {}

function button.new(parent,images,x,y,width,height)
  assert(type(x)=="number","scene.new - x must be a number")
  assert(type(y)=="number","scene.new - y must be a number")
  assert(type(width)=="number","scene.new - width must be a number")
  assert(type(height)=="number","scene.new - height must be a number")
  assert(type(images)=="table","scene.new - images must be a table")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  
  function instance:draw()
    local image = images.normal
    if self:isHover() then
      image = images.hover
    end
    love.graphics.setColor({255,255,255,255})
    love.graphics.draw(image,self:getX(),self:getY())
  end

  return instance
end

return button