local control = require "common.controls.control"
local images = require "game.images"
local mapCell = {}

function mapCell.new(parent,x,y,width,height,map,column,row)
  assert(type(x)=="number","mapCell.new - x must be a number")
  assert(type(y)=="number","mapCell.new - y must be a number")
  assert(type(width)=="number","mapCell.new - width must be a number")
  assert(type(height)=="number","mapCell.new - height must be a number")
  assert(type(column)=="number","mapCell.new - column must be a number")
  assert(type(row)=="number","mapCell.new - row must be a number")
  assert(type(map)=="table" and getmetatable(map)=="level","mapCell.new - map must be a level")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  
  local values = 
  {
    map = map,
    column=column,
    row=row
  }
  
  function instance:draw()
    local image=images.get(values.map:getCell(values.column,values.row))
    love.graphics.setColor({255,255,255,255})
    love.graphics.draw(image,self:getX(),self:getY())
    if self:isHover() then
      love.graphics.draw(images.get("cursor"),self:getX(),self:getY())
    end
  end
  
  function instance:getMap()
    return values.map
  end
  
  function instance:getMapColumn()
    return values.column
  end
  
  function instance:getMapRow()
    return values.row
  end
  
  return instance
end

return mapCell