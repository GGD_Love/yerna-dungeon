local control = require "common.controls.control"
local mapCellControl = require "common.controls.mapCell"
local mapEditor = {}

function mapEditor.new(parent,x,y,width,height,cellWidth,cellHeight)
  assert(type(x)=="number","mapEditor.new - x must be a number")
  assert(type(y)=="number","mapEditor.new - y must be a number")
  assert(type(width)=="number","mapEditor.new - width must be a number")
  assert(type(height)=="number","mapEditor.new - height must be a number")
  assert(type(cellWidth)=="number","mapEditor.new - cellWidth must be a number")
  assert(type(cellHeight)=="number","mapEditor.new - cellHeight must be a number")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  
  local values=
  {
    cellWidth = cellWidth,
    cellHeight = cellHeight,
    mapCells = {}
  }
  
  function instance:setMap(map)
    assert(type(map)=="nil" or (type(map)=="table" and getmetatable(map)=="level"),"mapEditor:setMap - map must be nil or a level")
    if values.map~=map then
      values.mapCells={}
      self:removeChildren()
      values.map=map
      if values.map~=nil then
        for column=1,values.map:getColumns() do
          for row=1,values.map:getRows() do
            local mapCell = mapCellControl.new(self,values.cellWidth*(column-1.5),values.cellHeight*(row-1.5),values.cellWidth,values.cellHeight,values.map,column,row)
          end
        end
      end
    end
  end
  
  function instance:preDraw()
    love.graphics.setScissor(self:getX(),self:getY(),self:getWidth(),self:getHeight())
  end
  
  function instance:postDraw()
    love.graphics.setScissor()
  end
  
  return instance
end

return mapEditor