local platform = {}

function platform.writeFile(fileName,text)
  assert(type(fileName)=="string","platform.writeFile - fileName must be a string")
  assert(type(text)=="string","platform.writeFile - text must be a string")
  
  love.filesystem.write(fileName,text)
end

function platform.readFile(fileName)
  assert(type(fileName)=="string","platform.readFile - fileName must be a string")
  
  return love.filesystem.read(fileName)
end

return require("utilities.proxy").newProxy(platform,"platform")